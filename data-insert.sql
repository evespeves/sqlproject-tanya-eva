use deliveryInformation;

-- Populate table

--Insert into customer
INSERT INTO customer VALUES(1,"Pearse Shilling","bob@hotmail.com", "33 Garratt Terrance, Tooting Broadway, London",07462280147);
INSERT INTO customer VALUES(2,"Mark Chinwag","chinwag@gmail.com","24 Blackforest Lane, Ealing, London",07843546453);
INSERT INTO customer VALUES(3,"SanMiguel Filmer","filmer@yahoo.com","2 Birchwood Rd.,Lincon Park, Kensington, London",0745245529);
INSERT INTO customer VALUES(4,"Tanya Sanatani","TanyaSanatani@gmail.com","Flat 1706, Heritage Tower, Crossharbour, London",07641698532);
INSERT INTO customer VALUES(5,"Eva Aherne","eva.aherne2@gmail.com","25 Raspberry Court, Stratford, London",07841692034);
INSERT INTO customer VALUES(6,"Mary Dwyer","mdwyer@hotmail.com", "22 Maggot Terrance, Tooting, London",07845136050);
INSERT INTO customer VALUES(7,"Eileen Aherne","eileenw365@gmail.com","24 BlackTree Lane, Elephant and Castle, London",078435487515);
INSERT INTO customer VALUES(8,"Tom Filmer","jellytots@yahoo.com","flat 56, 180 High Street, Stratford, London",07459368921);
INSERT INTO customer VALUES(9,"Frank Gallagher","frankyboy29@gmail.com","Flat 68, Liberty Tower, Blackwall, London",07678128262);
INSERT INTO customer VALUES(10,"Simon Pegg","spegg@gmail.com","217 Pegg Lane, Heathrow, London",07848745210);

--Insert into suppliers
INSERT INTO suppliers VALUES(1,"Deliveroo","deliveroo@hotmail.com", "Broadway, London",0214886120);
INSERT INTO suppliers VALUES(2,"Just Eat","just-eat@justeat.com","Ealing, London",02818956233);
INSERT INTO suppliers VALUES(3,"Uber Eats","uber-eats@yahoo.com","Kensington, London",027946656);
INSERT INTO suppliers VALUES(4,"GrubHub","grubhub@ghub.com","Crossharbour, London",02478932649);
INSERT INTO suppliers VALUES(5,"Meituan Waimai","meituan_waimai@mw.com","Stratford, London",0245692637);
INSERT INTO suppliers VALUES(6,"PizzaHut","pizzahut@pizza.com", "Tooting, London",07894962987);
INSERT INTO suppliers VALUES(7,"SnapFinger","SnapFinger@yahoo.com","Elephant and Castle, London",092878621);
INSERT INTO suppliers VALUES(8,"PostMates","postmates@mates.com","Stratford, London",0269721357);
INSERT INTO suppliers VALUES(9,"ShakeShack","ss@ShakeShack.com","Blackwall, London",0179656562);
INSERT INTO suppliers VALUES(10,"Carluccios","carolos@Carluccios.com","Heathrow, London",01979865332);

--Insert into paymentDetails
INSERT INTO paymentDetails VALUES(1,"Debit Card",1234567812345678, "2022-06-12 23:05:10", TRUE);
INSERT INTO paymentDetails VALUES(2,"Credit Card",2222333344445555, "2019-12-10 20:56:11", TRUE);
INSERT INTO paymentDetails VALUES(3,"Credit Card",2222333344445555, "2022-06-12 23:05:10", TRUE);
INSERT INTO paymentDetails VALUES(4,"Debit Card",9876987698769876, "2030-06-12 12:10:10", FALSE);
INSERT INTO paymentDetails VALUES(5,"Credit Card",3333444455556666, "2035-07-02 13:10:10", FALSE);
INSERT INTO paymentDetails VALUES(6,"Debit Card",1234567812345678, "2022-06-12 23:05:10", TRUE);
INSERT INTO paymentDetails VALUES(7,"Credit Card",2222333344445555, "2019-12-10 20:56:11", TRUE);
INSERT INTO paymentDetails VALUES(8,"Credit Card",2222333344445555, "2022-06-12 23:05:10", TRUE);
INSERT INTO paymentDetails VALUES(9,"Debit Card",9876987698769876, "2030-06-12 12:10:10", FALSE);
INSERT INTO paymentDetails VALUES(10,"Credit Card",3333444455556666, "2035-07-02 13:10:10", FALSE);

--Insert into driver
INSERT INTO driver VALUES(1,"Matt Davidson","mattyDav@hotmail.com", "Car",07989232926);
INSERT INTO driver VALUES(2,"Dave Harleyson","davyHarl@justeat.com","Ealing, London",02818956233);
INSERT INTO driver VALUES(3,"Frank Sinatra","frankySinatra@yahoo.com","Kensington, London",027946656);
INSERT INTO driver VALUES(4,"Rudhi Garg","garg.rudhi@ghub.com","Crossharbour, London",02478932649);
INSERT INTO driver VALUES(5,"Gorge Gillings","gillingsGorge@mw.com","Stratford, London",0245692637);
INSERT INTO driver VALUES(6,"Paur Yambolv","PaurYambolv@pizza.com", "Tooting, London",07894962987);
INSERT INTO driver VALUES(7,"Yanseul Yo","yoHans@yahoo.com","Elephant and Castle, London",092878621);
INSERT INTO driver VALUES(8,"Alisinder Tityat","alis_tit@mates.com","Stratford, London",0269721357);
INSERT INTO driver VALUES(9,"Stephaine Jelehan","sj@ShakeShack.com","Blackwall, London",0179656562);
INSERT INTO driver VALUES(10,"Nyasha Mutangadura","nyasha_m@gmail.com","Heathrow, London",01979865332);

--Insert into orderDetails
INSERT INTO orderDetails VALUES(1,10,10,8,2,"Pizza",2,30,"2017-06-12 23:05:10",12);
INSERT INTO orderDetails VALUES(2,10,9,5,3,"Pasta",1,6, "2018-12-10 20:56:11",5);
INSERT INTO orderDetails VALUES(3,8,8,2,7,"Burger",4,30, "2016-12-10 20:56:11",7);
INSERT INTO orderDetails VALUES(4,7,7,2,6,"Chips",3,10, "2019-01-10 20:56:11",3);
INSERT INTO orderDetails VALUES(5,1,6,10,5,"Wrap",2,8, "2015-12-10 20:56:11",3);
INSERT INTO orderDetails VALUES(6,1,5,6,4,"Tacos",1,20, "2019-06-10 20:56:11",19);
INSERT INTO orderDetails VALUES(7,2,4,4,3,"Sandwich",1,3, "2019-04-05 21:00:11",3);
INSERT INTO orderDetails VALUES(8,3,3,2,2,"Chicken Wings",1,8,"2014-11-13 20:56:11",7);
INSERT INTO orderDetails VALUES(9,4,2,1,2,"Brownie",1,10, "2018-10-10 21:56:11",9);
INSERT INTO orderDetails VALUES(10,5,1,3,1,"Waffles",5,45, "2017-05-10 14:56:11",8);

