-- SQL Script
DROP DATABASE IF EXISTS deliveryInformation;
CREATE DATABASE deliveryInformation;
use deliveryInformation;


CREATE TABLE customer (
	customerId INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	customerName VARCHAR(30) NOT NULL,
	customerEmail VARCHAR(50) NOT NULL,
	customerAddress VARCHAR(50) NOT NULL,
	customerPhoneNo BIGINT(11) NOT NULL
);

CREATE TABLE suppliers (
	supplierId INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	supplierName VARCHAR(30) NOT NULL,
	supplierEmail VARCHAR(50) NOT NULL,
	supplierAddress VARCHAR(50) NOT NULL,
	supplierPhoneNo BIGINT(11) NOT NULL
);

CREATE TABLE paymentDetails (
	paymentId INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	paymentType VARCHAR(20) NOT NULL,
	cardNo BIGINT(16) NOT NULL,
	expDate TIMESTAMP,
	paid BOOLEAN DEFAULT FALSE NOT NULL
);

CREATE TABLE driver (
	driverId INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	driverName VARCHAR(30) NOT NULL,
	driverEmail VARCHAR(30) NOT NULL,
	vehicleType VARCHAR(30) NOT NULL,
	driverPhone BIGINT(11) NOT NULL
);

CREATE TABLE orderDetails (
	orderId INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	customerId INT(11) UNSIGNED  NOT NULL,
	paymentId INT(11) UNSIGNED NOT NULL,
	driverId INT(11) UNSIGNED NOT NULL,
	supplierId INT(11) UNSIGNED NOT NULL,
	itemsOrdered VARCHAR(265),
	quantityOrdered INT(3) UNSIGNED,
	price INT(5) UNSIGNED,
	timeOrdered TIMESTAMP,
	unitPrice INT(5),
	
	FOREIGN KEY (customerId) 
        REFERENCES customer(customerId),
        
	FOREIGN KEY (paymentId) 
        REFERENCES paymentDetails(paymentId),
    
    FOREIGN KEY (supplierId) 
        REFERENCES suppliers(supplierId),
        
    FOREIGN KEY (driverId) 
        REFERENCES driver(driverId)
);

