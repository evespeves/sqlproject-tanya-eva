Team Members: Tanya Sanatani && Eva Aherne

Assumptions
1. It is assumed that there is one driver per order.
2. It is assumed that price in the order details includes delivery price.
3. 



Questions

-- SQL Script
use deliveryInformation;

/*
1   When was the last order made?
*/

Select * FROM orderDetails
ORDER BY timeOrdered DESC
LIMIT 1;

/*
2   Most expensive order?
*/

Select * FROM orderDetails
ORDER BY price DESC
LIMIT 1;

/*
3   Cheapest order?
*/

Select * FROM orderDetails
ORDER BY price
LIMIT 1;

/*
4  Most Popular Payment Type?
*/
SELECT paymentType,count(paymentType) FROM paymentDetails
GROUP BY paymentType;


/*
5   Show how many orders have been made per customer
*/

SELECT customerName, Count(orderId) AS num_of_orders FROM customer 
INNER JOIN orderDetails ON customer.customerId = orderDetails.customerId
GROUP BY orderDetails.customerId;

/*
6   Show how many orders have been made per supplier
*/

SELECT supplierName, Count(orderId) AS num_of_orders FROM suppliers
INNER JOIN orderDetails ON suppliers.supplierId = orderDetails.supplierId
GROUP BY orderDetails.supplierId;


/*
7   What customer orders what and from who
*/

SELECT customer.customerName, orderDetails.itemsOrdered, orderDetails.price AS TotalPrice, orderDetails.unitPrice,suppliers.supplierName FROM orderDetails
INNER JOIN customer ON customer.customerId = orderDetails.customerId
INNER JOIN suppliers ON suppliers.supplierId = orderDetails.supplierId
GROUP BY customer.customerName, orderDetails.itemsOrdered, suppliers.supplierName, orderDetails.price,orderDetails.unitPrice;

/*
8 Which Drivers deliver what supplier?
*/

SELECT driver.driverName, suppliers.supplierName FROM orderDetails
INNER JOIN driver ON driver.driverId = orderDetails.driverId
INNER JOIN suppliers ON suppliers.supplierId = orderDetails.supplierId
GROUP BY driver.driverName, suppliers.supplierName;

/*
9 How many Orders does each driver deliver and to whom?
*/

SELECT driver.driverName, customer.customerName FROM orderDetails
INNER JOIN driver ON driver.driverId = orderDetails.driverId
INNER JOIN customer ON customer.customerId = orderDetails.customerId
GROUP BY driver.driverName, customer.customerName;

/*
10 Payment Details of the most Expensive Order
*/

SELECT * FROM orderDetails
INNER JOIN paymentDetails ON orderDetails.PaymentId = PaymentDetails.PaymentId
INNER JOIN customer ON orderDetails.customerId = customer.customerId
ORDER BY orderDetails.price DESC
LIMIT 1;


